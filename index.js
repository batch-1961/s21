console.log("Hello World");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);


function addName(content) {
  users[6] = content;
}

addName("John Cena");
console.log(users);



/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/


// function ifind(place){
//     for (let index = 0; index < users.length; index++){
//         if(users[index] === place){
//              return users[index];
// }
// }
// }

// let itemFound = ifind(2);




/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/



/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/


/*
    5. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/


function showFoundItem(arg) {
  let found = users[arg]
  if (typeof found == 'undefined') {
  alert('Item not found')
 
  } else{ 
  alert(found + " found ")

  } 
  
 }
showFoundItem(1);



/*function displayUsers() {
   for (i = 0; i < users.length; i++)
   {
    if (users[users.length] < 0 ){
        alert("No item in the array");
        console.log("No item in the array")
    } else {
         console.log(users[i]);
  
    }
    }  
      console.log(users[i]);
}

       displayUsers();*/

function displayUser() {
  if (users.length <= 0){
    alert("No user")
  } else {
   for (let i = 0; i <= users.length; i++) {
      console.log(users[i]);
    }
  }
 
}

displayUser()
  